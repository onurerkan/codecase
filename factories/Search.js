/**
 * Search Local JSON
 * @author Onur Erkan
 */

angular.module('app').factory('getCountries', function($http) {
    return {
        getData: function(done) { 

            $http({
                method: 'GET',
                url: 'assets/countries.json'
             }).then(function (data){
                 done(data);
             },function (error){
                 console.log("request error");
             });

        }
    }
});

angular.module('app').factory('getContinents', function($http) {
    return {
        getData: function(done) { 

            $http({
                method: 'GET',
                url: 'assets/continents.json'
             }).then(function (data){
                 done(data);
             },function (error){
                 console.log("request error");
             });

        }
    }
});