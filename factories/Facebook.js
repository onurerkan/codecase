/**
 * Facebook Connect Factory
 * FBSDK
 * @author: Onur ERKAN
 */


/**
 * Facebook User Details
 * @param {type} param1
 * @param {type} param2
 */
angular.module('app').factory('facebookAPI', function($q) {
    return {
        getUserInfo: function() {
            var deferred = $q.defer();
            FB.api('/me', {
                fields: 'name,last_name'
            }, function(response) {
                if (!response || response.error) {
                    deferred.reject('Error occured');
                } else {
                    deferred.resolve(response);
                }
            });
            return deferred.promise;
        }
    }
});


/**
 * Facebook Login
 */

angular.module('app').factory('facebookLogin', function($q) {
    // Önce user login status'e bakıp sonra login sdk.js:58
    return {
        userLogin: function() {
            var deferred = $q.defer();
            
            FB.login(function(response) {
                if (!response || response.error) {
                    deferred.reject('Error occured');
                } else {
                    deferred.resolve(response);
                }
            });
             return deferred.promise;
        }
    }
});


/**
 * Facebook Logout
 */

angular.module('app').factory('facebookLogout', function($q) {
    // Önce user login status'e bakıp sonra login sdk.js:58
    return {
        userLogout: function() {
           FB.logout(function(response) {
            // user is now logged out
          });
        }
    }
});
