'use strict';
/**
 * Home.Controller
 * @param {type} name description
 */

angular.module('app')
        .controller('HomeCtrl',['$rootScope','$window','$scope','$http','facebookLogin','facebookAPI', function($rootScope,$window,$scope,$http,facebookLogin,facebookAPI){
            console.log("Home Controller initialized.");
            
            $scope.current = "HOME.HOME";
            $scope.button = "Get Last Name";
            $scope.login =  function(){
                facebookLogin.userLogin().then(function(response){
                    console.log(response);
                    if(response.status == "connected"){
                        $scope.isLogged = true;
                        $rootScope.isLogged = true;
                        $rootScope.userID = response.authResponse.userID; //Cookie,LocalStorage,WebSQL
                        facebookAPI.getUserInfo() 
                                .then(function(response) {
                                  $rootScope.name = response.name;
                                  $window.location.href = '#!/dashboard';
                                }
                              );
                        
                        
                        
                        
                    }
                        
                });
            };
}]);
