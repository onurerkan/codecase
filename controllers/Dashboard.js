/**
 * Dashboard Controller
 */

angular.module('app')
        .controller('DashboardCtrl',['$rootScope','$window','$scope','$http','facebookAPI','getCountries','getContinents','facebookLogout', function($rootScope,$window,$scope,$http,facebookAPI,getCountries,getContinents,facebookLogout){
                $scope.isLogged = $rootScope.isLogged;
                $scope.userInfo = $rootScope.name;
                $scope.isVisible = false;
                $scope.selectedFromSearch = '';
         
        // When dom is ready        
        angular.element(document).ready(function () {
       
       
            var fills = {
                'USA': '#1f77b4',
                defaultFill: '#00ff00',
                export : '#0027ff',
                import : '#ffc0cb'
            };
            var map = new Datamap({element: document.getElementById('mapz'), fills: fills, responsive: true,
                scope: 'world',
                data: {
                 'USA': {fillKey: 'defaultFill'}
                },
                done: function(datamap) {
                $scope.datamap = datamap;    
                datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
                    alert(geography.properties.name);
                });
                $scope.mapRight = function(){
                    datamap.svg.selectAll('.datamaps-subunit').on('contextmenu', function(geography) {
                        $scope.$apply(function () {
                            $scope.isVisible = true;
                       });
                       $scope.selectedCountry = geography;
                        return false;
                    },false);
                };
             
            }});
        
            $scope.fromSearch = function(bu){
                var code = bu.country.code;
                $scope.selectedFromSearch  = code;
                $scope.isVisible = true;
                $scope.selectedCountry = { id : code };
            };
            
            $scope.makeExport = function(){
                var geography =  $scope.selectedCountry;
                var datamap = $scope.datamap;
                $scope.exportCount += 1;
                var new_fills = {
                  [geography.id] : {
                    fillKey: 'export'
                  }
                };
                
                datamap.updateChoropleth(new_fills);
            
            };
            
            $scope.makeImport = function(){
                var geography =  $scope.selectedCountry;
                var datamap = $scope.datamap;
                var new_fills = {
                  [geography.id] : {
                    fillKey: 'import'
                  }
                };
                datamap.updateChoropleth(new_fills);
                $scope.importCount += 1;
            };
            
            
            getCountries.getData(function(response){ 
                    $scope.countries = response.data["data"];
                    $scope.allCount = response.data["data"].length;
                    $scope.exportCount = 0;
                    $scope.importCount = 0;
                });
            
            
            $scope.results = [];
            $scope.findCountry = function(val) {  
            angular.forEach($scope.countries, function(value, key) {
                if (value.name == val) {
                    
                }
            });
            
            };
        
        
            // d3 Responsive
            d3.select(window).on('resize', function() {
                map.resize();
            });
            
            
            getContinents.getData(function(response){ 
                    $scope.continents = response.data["continentsList"];
            });
            
            $scope.logout = function(){
                facebookLogout.userLogout();
                $scope.isLogged = false;
                $rootScope.isLogged = false;
                $window.location.href = '#!/home';
            }
       
    });
    
           
        
}]).directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});