/**
 * Continent Controller
 * @param {type} $rootScope
 * @param {type} $window
 * @param {type} $scope
 * @param {type} $http
 * @param {type} routeParams
 * @returns {undefined}
 */
angular.module('app')
        .controller('ContinentCtrl',['$rootScope','$window','$scope','$http','$route','getContinents', function($rootScope,$window,$scope,$http,$route,getContinents){
        
            var continent = $route.current.params.continent;
            
            getContinents.getData(function(response){ 
                    $scope.list = [];
                    var countries = response.data["countries"];
                   
                    angular.forEach(countries, function(value, key) {
                        if (value.continent == continent) {
                             $scope.list.push({name:value.name, capital: value.capital, currency: value.currency, phone: value.phone, flag:key.toLowerCase()});
                        }
                    });
                    console.log($scope.list);
            });
    
}]);