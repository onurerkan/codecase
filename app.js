'use strict';
/**
 * Case for Sahibinden.com
 * @author : Onur Erkan
 */


let modules = ['ngRoute'];
var app = angular.module('app',modules);
/**
 * Configuration & Router
 */
app.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {
    $routeProvider
    // Login
    .when("/dashboard", {
        templateUrl: "views/dashboard.html"
       ,controller: "DashboardCtrl"
        })
    .when('/continent/:continent', {
        templateUrl: "views/continent.html"
        ,controller : "ContinentCtrl"
    }) 
    // Default
    .otherwise("/home", {
        redirectTo: "index.html"});
    
}]);
/** 
 * When page load 
 */
app.run(['$rootScope','$window','$location','$timeout', function($rootScope,$window,$location,$timeout){
     
     $rootScope.user = {};
     $rootScope.isLogged = false;
     
     $timeout(function(){
        (function(d){
        // load the Facebook javascript SDK

        var js,
        id = 'facebook-jssdk',
        ref = d.getElementsByTagName('script')[0];

        if (d.getElementById(id)) {
          return;
        }

        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/sdk.js";

        ref.parentNode.insertBefore(js, ref);

      }(document));
        $window.fbAsyncInit = function() {
        FB.init({ 
          appId: '1393762684273459',
          status: true, 
          cookie: true, 
          xfbml: true,
          version: 'v2.4'
        });

        
        };
        console.log("TIMEOUT");
     }, 1000);
     
     

    
    //Check if user logged in
         if ($location.path() !== '#!/dashboard' && !$rootScope.userID) {
                $location.path('#!/home');
            }
    
    
        
}]);


